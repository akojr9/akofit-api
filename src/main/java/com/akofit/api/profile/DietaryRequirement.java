package com.akofit.api.profile;

import java.util.HashMap;
import java.util.Map;

public enum DietaryRequirement {
    ANYTHING, VEGETARIAN, VEGAN;

    private static Map<String, DietaryRequirement> valueMap;

    public static DietaryRequirement getValue(String possibleName) {
        if (valueMap == null) {
            valueMap = new HashMap<>();
            for (DietaryRequirement dietaryRequirement : values())
                valueMap.put(dietaryRequirement.toString(), dietaryRequirement);
        }
        return valueMap.get(possibleName);
    }
}