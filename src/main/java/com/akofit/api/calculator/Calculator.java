package com.akofit.api.calculator;

import static com.akofit.api.common.CommonAppUtils.round;

public class Calculator {
    public static double getBaseMetabolicRate(double baseRate, double weightMultiplier, double heightMultiplier, double ageMultiplier, int age, int weight, int height) {
        double weightPart = weightMultiplier * weight;
        double heightPart = heightMultiplier * height;
        double agePart = ageMultiplier * age;
        return round(baseRate + heightPart + weightPart + agePart, 1);
    }
}
