package com.akofit.api;

import com.akofit.api.recipe.RecipeReader;
import com.akofit.api.recipe.RecipeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StartupHousekeeper {
    private RecipeService recipeService;

    public StartupHousekeeper(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void contextRefreshedEvent() {
        log.info("Loading Akofit Api...");
        ClassPathResource csvResource = new ClassPathResource("src/main/resources/data/recipes-small.csv");
        RecipeReader recipeReader = new RecipeReader(csvResource.getPath(), "", "}", recipeService);
        recipeReader.readCsv();
    }
}