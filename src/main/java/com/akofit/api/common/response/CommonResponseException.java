package com.akofit.api.common.response;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class CommonResponseException extends Exception {

    private static final long serialVersionUID = 2306275390425764500L;

    private HttpStatus status;

    public CommonResponseException(String error, HttpStatus status) {
        super(error);
        this.status = status;
    }
}