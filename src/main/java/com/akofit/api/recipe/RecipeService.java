package com.akofit.api.recipe;

import com.akofit.api.profile.DietaryRequirement;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.akofit.api.common.CommonAppUtils.readFile;
import static com.akofit.api.common.CommonAppUtils.round;
import static com.akofit.api.recipe.Recipe.pickNRandomRecipes;

@Service
@Slf4j
public class RecipeService {
    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    List<Recipe> getRecipes() {
        List<Recipe> recipes = new ArrayList<>();
        recipeRepository.findAll().forEach(recipes::add);
        return recipes;
    }

    private void addRecipe(Recipe recipe) {
        recipeRepository.save(recipe);
    }

    double countRecipes() {
        return recipeRepository.count();
    }

    void addRecipes(List<Recipe> recipeList) {
        if (recipeList != null) {
            for (Recipe recipe : recipeList) {
                addRecipe(recipe);
            }
        }
    }

    List<Recipe> generateMealPlan(String dietaryRequirement, double calories, int meals) {
        List<Recipe> recipeList = new ArrayList<>();
        recipeRepository.findAll().forEach(recipeList::add);

        // Remove recipes with excessive calories

        List<Recipe> recipesToDelete = new ArrayList<>();
        for (Recipe recipe : recipeList) {
            if (recipe.getCalories() > calories) {
                recipesToDelete.add(recipe);
            }
        }
        for (Recipe recipeToDelete : recipesToDelete) {
            recipeList.remove(recipeToDelete);
        }

        // Apply dietary constraints
        // Non vegan source: https://github.com/hmontazeri/is-vegan/blob/master/src/util/nonvegan.json

        if (dietaryRequirement.equalsIgnoreCase(String.valueOf(DietaryRequirement.VEGAN))) {
            ClassPathResource nonVeganResource = new ClassPathResource("src/main/resources/data/nonvegan.json", this.getClass().getClassLoader());
            String nonVeganJsonData = readFile(nonVeganResource.getPath());
            JSONArray nonVeganItems = new JSONArray(nonVeganJsonData);
            recipesToDelete = new ArrayList<>();
            pruneRecipeList(recipeList, recipesToDelete, nonVeganItems);
        }

        if (dietaryRequirement.equalsIgnoreCase(String.valueOf(DietaryRequirement.VEGETARIAN))) {
            ClassPathResource nonVegetarianResource = new ClassPathResource("src/main/resources/data/nonvegetarian.json", this.getClass().getClassLoader());
            String nonVegetarianJsonData = readFile(nonVegetarianResource.getPath());
            JSONArray nonVegetarianItems = new JSONArray(nonVegetarianJsonData);
            recipesToDelete = new ArrayList<>();
            pruneRecipeList(recipeList, recipesToDelete, nonVegetarianItems);
        }

        // Apply calories constraint

        List<Recipe> selectedMeals = new ArrayList<>();
        double selectedMealCalories = 0;
        boolean mealFound = false;
        while (!mealFound) {
            selectedMeals = pickNRandomRecipes(recipeList, meals);
            selectedMealCalories = selectedMeals.stream().mapToDouble(Recipe::getCalories).sum();
            double upperCaloriesBound = calories + (calories * 0.1);
            double lowerCaloriesBound = calories - (calories * 0.1);
            if (selectedMealCalories < upperCaloriesBound && selectedMealCalories > lowerCaloriesBound) {
                mealFound = true;
            }
        }
        log.debug("Selected {} meals with {} calories", selectedMeals.size(), round(selectedMealCalories, 1));

        for (Recipe selectedMeal : selectedMeals) {
            log.debug("Picked meal '{}'", selectedMeal.getName().trim());
        }

        return selectedMeals;
    }

    private void pruneRecipeList(List<Recipe> recipeList, List<Recipe> recipesToDelete, JSONArray items) {
        for (Recipe recipe : recipeList) {
            String recipeIngredients = recipe.getIngredients().toUpperCase();
            for (int j = 0; j < items.length(); j++) {
                String nonVeganItem = items.get(j).toString().toUpperCase();
                if (recipeIngredients.contains(nonVeganItem)) {
                    recipesToDelete.add(recipe);
                }
            }
        }
        for (Recipe recipeToDelete : recipesToDelete) {
            recipeList.remove(recipeToDelete);
        }
    }
}