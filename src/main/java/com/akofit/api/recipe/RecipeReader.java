package com.akofit.api.recipe;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.akofit.api.common.CommonAppUtils.round;

@Slf4j
public class RecipeReader {
    private final String csvFile;
    private final String csvSplitBy;
    private final RecipeService recipeService;
    private String line;

    public RecipeReader(String csvFile, String line, String csvSplitBy, RecipeService recipeService) {
        this.csvFile = csvFile;
        this.line = line;
        this.csvSplitBy = csvSplitBy;
        this.recipeService = recipeService;
    }

    public void readCsv() {
        int errorCount = 0;
        List<Recipe> recipeList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int iteration = 0;
            while ((line = br.readLine()) != null) {
                if (iteration == 0) {
                    iteration++;
                    continue;
                }
                String[] recipeProperty = line.split(csvSplitBy);
                try {
                    Recipe recipe = new Recipe();
                    recipe.setName(recipeProperty[0]);
                    recipe.setServings(Double.parseDouble(recipeProperty[2]));
                    recipe.setIngredients(recipeProperty[3]);
                    recipe.setDirections(recipeProperty[4]);

                    double servings = Double.parseDouble(recipeProperty[2]);
                    double fat = Double.parseDouble(recipeProperty[5].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", ""));
                    double saturatedFat = Double.parseDouble(recipeProperty[6].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", ""));
                    double carbohydrates = Double.parseDouble(recipeProperty[10].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", ""));
                    double protein = Double.parseDouble(recipeProperty[12].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", ""));
                    double sugar = Double.parseDouble(recipeProperty[13].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", ""));
                    double calories = ((fat * 9) + (saturatedFat * 9) + (carbohydrates * 4) + (protein * 4) + (sugar * 3.87) / servings);

                    recipe.setCalories(round(calories, 1));
                    recipe.setFat(fat);
                    recipe.setSaturatedFat(saturatedFat);
                    recipe.setCholesterol(Double.parseDouble(recipeProperty[7].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setSodium(Double.parseDouble(recipeProperty[8].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setPotassium(Double.parseDouble(recipeProperty[9].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setCarbohydrates(carbohydrates);
                    recipe.setFibre(Double.parseDouble(recipeProperty[11].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setProtein(protein);
                    recipe.setSugar(sugar);
                    recipe.setVitaminA(Double.parseDouble(recipeProperty[14].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setVitaminC(Double.parseDouble(recipeProperty[15].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setCalcium(Double.parseDouble(recipeProperty[16].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setIron(Double.parseDouble(recipeProperty[17].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setThiamin(Double.parseDouble(recipeProperty[18].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setNiacin(Double.parseDouble(recipeProperty[19].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setVitaminB6(Double.parseDouble(recipeProperty[20].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setMagnesium(Double.parseDouble(recipeProperty[21].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setFolate(Double.parseDouble(recipeProperty[22].replace("g", "").replace("m", "").replace("c", "").replace("I", "").replace("U", "")));
                    recipe.setImg(recipeProperty[23]);
                    recipe.setTags(recipeProperty[24]);
                    String message = String.format("Successfully added recipe '%s'", recipeProperty[0]);
                    log.debug(message);
                    recipeList.add(recipe);
                } catch (Exception e) {
                    String error = String.format("Error occurred at recipe '%s'", recipeProperty[0]);
                    log.error(error);
                    log.error(e.getMessage());
                    errorCount++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        recipeService.addRecipes(recipeList);
        String message = String.format("Added %s recipes with %s errors", recipeList.size(), errorCount);
        log.info(message);
    }
}