package com.akofit.api.profile;

import java.util.HashMap;
import java.util.Map;

public enum Gender {
    MALE, FEMALE;

    private static Map<String, Gender> valueMap;

    public static Gender getValue(String possibleName) {
        if (valueMap == null) {
            valueMap = new HashMap<>();
            for (Gender gender : values())
                valueMap.put(gender.toString(), gender);
        }
        return valueMap.get(possibleName);
    }
}
