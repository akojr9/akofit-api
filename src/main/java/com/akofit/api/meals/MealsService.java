package com.akofit.api.meals;

import com.akofit.api.recipe.Recipe;
import com.akofit.api.recipe.RecipeRepository;
import com.akofit.api.recipe.RecipeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.akofit.api.recipe.Recipe.pickNRandomRecipes;

@Service
@Slf4j
public class MealsService {
    private final RecipeRepository recipeRepository;
    private final RecipeService recipeService;

    @Autowired
    public MealsService(RecipeRepository recipeRepository, RecipeService recipeService) {
        this.recipeRepository = recipeRepository;
        this.recipeService = recipeService;
    }

    public List<Recipe> generateMeals() {
        List<Recipe> recipeList = new ArrayList<>();
        recipeRepository.findAll().forEach(recipeList::add);

        // Get breakfast meal candidates

        List<Recipe> breakfastMeals = new ArrayList<>();
        for (Recipe recipe : recipeList) {
            if (recipe.getCalories() < 400) {
                log.debug("Picked breakfast meal '{}'", recipe.getName().trim());
                breakfastMeals.add(recipe);
            }
        }
        List<Recipe> selectedBreakfastMeal = Recipe.pickNRandomRecipes(breakfastMeals, 1);

        List<Recipe> lunchMeals = new ArrayList<>();
        for (Recipe recipe : recipeList) {
            if (recipe.getCalories() < 700) {
                log.debug("Picked lunch meal '{}'", recipe.getName().trim());
                lunchMeals.add(recipe);
            }
        }
        List<Recipe> selectedLunchMeal = Recipe.pickNRandomRecipes(lunchMeals, 1);

        List<Recipe> dinnerMeals = new ArrayList<>();
        for (Recipe recipe : recipeList) {
            if (recipe.getCalories() < 700) {
                log.debug("Picked dinner meal '{}'", recipe.getName().trim());
                dinnerMeals.add(recipe);
            }
        }
        List<Recipe> selectedDinnerMeal = Recipe.pickNRandomRecipes(dinnerMeals, 1);

        return Stream.of(selectedBreakfastMeal, selectedLunchMeal, selectedDinnerMeal)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public int countMeals() {
        return generateMeals().size();
    }
}
