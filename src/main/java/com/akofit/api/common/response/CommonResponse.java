package com.akofit.api.common.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;

@Setter
@Getter
@JsonInclude(Include.NON_NULL)
public class CommonResponse {
    private String message;
    private String error;
    private Integer status;
    private String host;
    private String path;
    private Object data;

    public CommonResponse(Object data, String message, HttpStatus status, HttpServletRequest request) {
        setData(data);
        setHost(request.getServerName() + ":" + request.getServerPort());
        setPath(request.getRequestURI());
        setMessage(message);
        setStatus(status.value());
    }

    public CommonResponse(String error, HttpStatus status, HttpServletRequest request) {
        setError(error);
        setHost(request.getServerName() + ":" + request.getServerPort());
        setStatus(status.value());
        setPath(request.getRequestURI());
    }
}