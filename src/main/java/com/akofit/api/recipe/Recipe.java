package com.akofit.api.recipe;

import lombok.Getter;
import lombok.Setter;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Getter
@Setter
@Entity
@Table(name = "recipes")
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false, unique = true)
    private Integer id;

    @Unique
    @NotBlank
    @Column(name = "name", columnDefinition = "TEXT")
    private String name;

    @Column(name = "servings", columnDefinition = "TEXT")
    private double servings;

    @Column(name = "ingredients", columnDefinition = "TEXT")
    private String ingredients;

    @Column(name = "directions", columnDefinition = "TEXT")
    private String directions;

    @Column(name = "calories", columnDefinition = "DOUBLE")
    private double calories;

    @Column(name = "fat", columnDefinition = "DOUBLE")
    private double fat;

    @Column(name = "saturated_fat", columnDefinition = "DOUBLE")
    private double saturatedFat;

    @Column(name = "cholestrol", columnDefinition = "DOUBLE")
    private double cholesterol;

    @Column(name = "sodium", columnDefinition = "DOUBLE")
    private double sodium;

    @Column(name = "potassium", columnDefinition = "DOUBLE")
    private double potassium;

    @Column(name = "carbohydrates", columnDefinition = "DOUBLE")
    private double carbohydrates;

    @Column(name = "fibre", columnDefinition = "DOUBLE")
    private double fibre;

    @Column(name = "protein", columnDefinition = "DOUBLE")
    private double protein;

    @Column(name = "sugar", columnDefinition = "DOUBLE")
    private double sugar;

    @Column(name = "vitamin_a", columnDefinition = "DOUBLE")
    private double vitaminA;

    @Column(name = "vitamin_c", columnDefinition = "DOUBLE")
    private double vitaminC;

    @Column(name = "calcium", columnDefinition = "DOUBLE")
    private double calcium;

    @Column(name = "iron", columnDefinition = "DOUBLE")
    private double iron;

    @Column(name = "thiamin", columnDefinition = "DOUBLE")
    private double thiamin;

    @Column(name = "niacin", columnDefinition = "DOUBLE")
    private double niacin;

    @Column(name = "vitamin_b6", columnDefinition = "DOUBLE")
    private double vitaminB6;

    @Column(name = "magnesium", columnDefinition = "DOUBLE")
    private double magnesium;

    @Column(name = "folate", columnDefinition = "DOUBLE")
    private double folate;

    @Column(name = "img", columnDefinition = "TEXT")
    private String img;

    @Column(name = "tags", columnDefinition = "TEXT")
    private String tags;

    // Source : https://stackoverflow.com/questions/4702036/take-n-random-elements-from-a-liste

    public static <E> List<E> pickNRandomRecipes(List<E> list, int n, Random r) {
        int length = list.size();

        if (length < n) return null;
        for (int i = length - 1; i >= length - n; --i) {
            Collections.swap(list, i, r.nextInt(i + 1));
        }
        return list.subList(length - n, length);
    }

    public static <E> List<E> pickNRandomRecipes(List<E> list, int n) {
        return pickNRandomRecipes(list, n, ThreadLocalRandom.current());
    }
}