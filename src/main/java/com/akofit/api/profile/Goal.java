package com.akofit.api.profile;

import java.util.HashMap;
import java.util.Map;

public enum Goal {
    GAIN, MAINTAIN, LOSE;

    private static Map<String, Goal> valueMap;

    public static Goal getValue(String possibleName) {
        if (valueMap == null) {
            valueMap = new HashMap<>();
            for (Goal weightGoal : values())
                valueMap.put(weightGoal.toString(), weightGoal);
        }
        return valueMap.get(possibleName);
    }
}
