package com.akofit.api.meals;

import com.akofit.api.common.response.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
public class MealsController {
    private final MealsService mealsService;

    public MealsController(MealsService mealsService) {
        this.mealsService = mealsService;
    }

    @GetMapping("/meals")
    public CommonResponse getAllRecipes(HttpServletRequest request) {
        Object data = mealsService.generateMeals();
        String message = String.format("Retrieved %s meals", mealsService.countMeals());
        return new CommonResponse(data, message, HttpStatus.OK, request);
    }
}
