package com.akofit.api.user;

import com.akofit.api.common.response.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;

@Slf4j
@CrossOrigin
@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public CommonResponse registerUser(HttpServletRequest request, @RequestBody User user) {
        if (userService.find(user.getUsername()) != null) {
            String message = String.format("User already '%s' already exists", user.getUsername());
            return new CommonResponse(message, HttpStatus.BAD_REQUEST, request);
        } else {
            user.setRole("USER");
            Object data = userService.save(user);
            String message = String.format("User '%s' created", user.getUsername());
            return new CommonResponse(data, message, HttpStatus.CREATED, request);
        }
    }

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public CommonResponse getUserAccount(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization").substring("Basic".length()).trim();
        String username = new String(Base64.getDecoder().decode(authToken)).split(":")[0];
        if (userService.find(username) != null) {
            String message = "User found";
            return new CommonResponse(userService.find(username), message, HttpStatus.CREATED, request);
        }
        String message = "User not found";
        return new CommonResponse(message, HttpStatus.BAD_REQUEST, request);
    }

    @RequestMapping(value = "/login")
    public CommonResponse login(HttpServletRequest request, @RequestBody User user) {
        if (userService.find(user.getUsername()) != null) {
            if (BCrypt.checkpw(user.getPassword(), userService.find(user.getUsername()).getPassword())) {
                String message = "Authentication success";
                return new CommonResponse(userService.find(user.getUsername()), message, HttpStatus.CREATED, request);
            }
        }
        String message = "There was an issue with the username or password you provided. Please try again.";
        return new CommonResponse(message, HttpStatus.BAD_REQUEST, request);
    }
}
