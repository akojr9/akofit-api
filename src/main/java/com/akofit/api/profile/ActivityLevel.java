package com.akofit.api.profile;

import java.util.HashMap;
import java.util.Map;

public enum ActivityLevel {
    SEDENTARY, LIGHT, MODERATE, ACTIVE, EXTREME;

    private static Map<String, ActivityLevel> valueMap;

    public static ActivityLevel getValue(String possibleName) {
        if (valueMap == null) {
            valueMap = new HashMap<>();
            for (ActivityLevel activityLevel : values())
                valueMap.put(activityLevel.toString(), activityLevel);
        }
        return valueMap.get(possibleName);

    }
}