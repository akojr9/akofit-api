package com.akofit.api.recipe;

import com.akofit.api.calculator.Calculator;
import com.akofit.api.common.response.CommonResponse;
import com.akofit.api.profile.ActivityLevel;
import com.akofit.api.profile.DietaryRequirement;
import com.akofit.api.profile.Gender;
import com.akofit.api.profile.Goal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.akofit.api.common.CommonAppUtils.round;

@Slf4j
@RestController
public class RecipeController {
    private final RecipeService recipeService;

    @Autowired
    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @RequestMapping("/recipes")
    @SuppressWarnings("unused")
    public String wranglerApiRequest() {
        return "Greetings from Akofit!";
    }

    @GetMapping("/recipes")
    public CommonResponse getAllRecipes(HttpServletRequest request) {
        Object data = recipeService.getRecipes();
        String message = String.format("Retrieved %s recipes", recipeService.countRecipes());
        return new CommonResponse(data, message, HttpStatus.OK, request);
    }

    @RequestMapping(path = "/generate", method = RequestMethod.POST)
    public CommonResponse generateMealPlan(HttpServletRequest request,
                                           @RequestParam(value = "age") String ageString,
                                           @RequestParam(value = "weight") String weightString,
                                           @RequestParam(value = "height") String heightString,
                                           @RequestParam(value = "gender") String gender,
                                           @RequestParam(value = "goal") String goal,
                                           @RequestParam(value = "activityLevel") String activityLevel,
                                           @RequestParam(value = "dietaryRequirement") String dietaryRequirement,
                                           @RequestParam(value = "calories") String caloriesString,
                                           @RequestParam(value = "meals") String mealsString) {
        log.debug("Search parameters found. Calories: {}, Meals: {}", caloriesString, mealsString);

        int age = Integer.parseInt(ageString);
        int weight = Integer.parseInt(weightString);
        int height = Integer.parseInt(heightString);
        double calories = Double.parseDouble(caloriesString);
        int meals = Integer.parseInt(mealsString);

        List<Recipe> data;
        String message;

        // Validation

        if (gender == null) {
            throw new IllegalArgumentException("Required parameter 'gender' is not set");
        }
        if (goal == null) {
            throw new IllegalArgumentException("Required parameter 'goal' is not set");
        }
        if (activityLevel == null) {
            throw new IllegalArgumentException("Required parameter 'activity level' is not set");
        }
        if (dietaryRequirement == null) {
            throw new IllegalArgumentException("Required parameter 'dietary requirement' is not set");
        }

        if (Gender.getValue(gender.toUpperCase().trim()) == null) {
            throw new IllegalArgumentException("Required parameter 'gender' is invalid");
        }
        if (Goal.getValue(goal.toUpperCase().trim()) == null) {
            throw new IllegalArgumentException("Required parameter 'goal' is invalid");
        }
        if (ActivityLevel.getValue(activityLevel.toUpperCase().trim()) == null) {
            throw new IllegalArgumentException("Required parameter 'activity level' is invalid");
        }
        if (DietaryRequirement.getValue(dietaryRequirement.toUpperCase().trim()) == null) {
            throw new IllegalArgumentException("Required parameter 'dietary requirement' is invalid");
        }

        // Calculate base metabolic rate

        double baseMetabolicRate = 0;
        if (gender.equalsIgnoreCase(Gender.MALE.toString())) {
            baseMetabolicRate = Calculator.getBaseMetabolicRate(66.47, 13.75, 5.0, 6.75, age, weight, height);
        }
        if (gender.equalsIgnoreCase(Gender.FEMALE.toString())) {
            baseMetabolicRate = Calculator.getBaseMetabolicRate(65.09, 9.56, 1.84, 4.67, age, weight, height);
        }
        baseMetabolicRate = round(baseMetabolicRate, 1);
        log.debug("Base metabolic rate: {}", baseMetabolicRate);

        // Calculate maintenance calories

        double maintenanceCalories = 0;
        if (activityLevel.equalsIgnoreCase(ActivityLevel.SEDENTARY.toString())) {
            maintenanceCalories = baseMetabolicRate * 1.2;
        } else if (activityLevel.equalsIgnoreCase(ActivityLevel.LIGHT.toString())) {
            maintenanceCalories = baseMetabolicRate * 1.375;
        } else if (activityLevel.equalsIgnoreCase(ActivityLevel.MODERATE.toString())) {
            maintenanceCalories = baseMetabolicRate * 1.55;
        } else if (activityLevel.equalsIgnoreCase(ActivityLevel.ACTIVE.toString())) {
            maintenanceCalories = baseMetabolicRate * 1.725;
        } else if (activityLevel.equalsIgnoreCase(ActivityLevel.EXTREME.toString())) {
            maintenanceCalories = baseMetabolicRate * 1.9;
        }

        if (goal.equalsIgnoreCase(Goal.GAIN.toString())) {
            maintenanceCalories = round(maintenanceCalories + 500, 1);
        } else if (goal.equalsIgnoreCase(Goal.LOSE.toString())) {
            maintenanceCalories = round(maintenanceCalories - 200, 1);
        }

        // Calculate macro-nutrient split

        double protein = round(maintenanceCalories * 0.35, 1);
        double carbohydrate = round(maintenanceCalories * 0.35, 1);
        double fat = round(maintenanceCalories * 0.3, 1);

        log.debug("Maintenance calories: {}", maintenanceCalories);
        log.debug("Protein: {}", protein);
        log.debug("Carbohydrate: {}", carbohydrate);
        log.debug("Fat: {}", fat);

        if (meals == 1 && calories < 200) {
            throw new IllegalArgumentException("For 1 meal, please enter a value greater than 200 calories");
        } else if (meals == 2 && calories < 400) {
            throw new IllegalArgumentException("For 2 meals, please enter a value greater than 400 calories");
        } else if (meals == 3 && calories < 600) {
            throw new IllegalArgumentException("For 3 meals, please enter a value greater than 600 calories");
        } else if (meals == 4 && calories < 800) {
            throw new IllegalArgumentException("For 4 meals, please enter a value greater than 800 calories");
        } else if (meals == 5 && calories < 1000) {
            throw new IllegalArgumentException("For 5 meals, please enter a value greater than 1000 calories");
        } else if (meals == 6 && calories < 1200) {
            throw new IllegalArgumentException("For 6 meals, please enter a value greater than 1200 calories");
        } else if (meals == 7 && calories < 1400) {
            throw new IllegalArgumentException("For 7 meals, please enter a value greater than 1400 calories");
        } else if (meals == 8 && calories < 1600) {
            throw new IllegalArgumentException("For 8 meals, please enter a value greater than 1600 calories");
        } else if (meals == 9 && calories < 1800) {
            throw new IllegalArgumentException("For 9 meals, please enter a value greater than 1800 calories");
        } else if (meals > 9) {
            throw new IllegalArgumentException("You can only generate a plan for 9 meals or less");
        }

        data = recipeService.generateMealPlan(dietaryRequirement, calories, meals);
        double totalMealCalories = round(data.stream().mapToDouble(Recipe::getCalories).sum(), 1);
        message = String.format("Retrieved %s recipes of %s calories to eat in %s meals", data.size(), totalMealCalories, meals);

        return new CommonResponse(data, message, HttpStatus.CREATED, request);
    }
}